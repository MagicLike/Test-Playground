47  <Q                         INSTANCING_ON      SHADOWS_DEPTH       a1  #ifdef VERTEX
#version 330
#extension GL_ARB_explicit_attrib_location : require
#ifndef UNITY_RUNTIME_INSTANCING_ARRAY_SIZE
	#define UNITY_RUNTIME_INSTANCING_ARRAY_SIZE 2
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 unity_LightShadowBias;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	int unity_BaseInstanceID;
uniform 	float _InitialBend;
uniform 	float _Stiffness;
uniform 	float _Drag;
uniform 	float _ShiverDrag;
uniform 	float _ShiverDirectionality;
uniform 	float _WindNormalInfluence;
uniform 	vec4 _NewNormal;
uniform 	vec4 WIND_SETTINGS_WorldDirectionAndSpeed;
uniform 	float WIND_SETTINGS_FlexNoiseScale;
uniform 	float WIND_SETTINGS_ShiverNoiseScale;
uniform 	float WIND_SETTINGS_Turbulence;
uniform 	float WIND_SETTINGS_GustSpeed;
uniform 	float WIND_SETTINGS_GustScale;
uniform 	float WIND_SETTINGS_GustWorldScale;
uniform 	vec4 _texcoord_ST;
struct unity_Builtins0Array_Type {
	vec4 hlslcc_mtx4x4unity_ObjectToWorldArray[4];
	vec4 hlslcc_mtx4x4unity_WorldToObjectArray[4];
};
UNITY_BINDING(0) uniform UnityInstancing_PerDraw0 {
	unity_Builtins0Array_Type unity_Builtins0Array[UNITY_RUNTIME_INSTANCING_ARRAY_SIZE];
};
UNITY_LOCATION(1) uniform  sampler2D WIND_SETTINGS_TexNoise;
UNITY_LOCATION(2) uniform  sampler2D WIND_SETTINGS_TexGust;
in  vec4 in_POSITION0;
in  vec3 in_NORMAL0;
in  vec4 in_TEXCOORD0;
in  vec4 in_COLOR0;
out vec2 vs_TEXCOORD1;
out vec3 vs_TEXCOORD2;
flat out uint vs_SV_InstanceID0;
vec4 u_xlat0;
int u_xlati0;
vec3 u_xlat1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec3 u_xlat7;
float u_xlat8;
float u_xlat9;
vec3 u_xlat10;
bvec3 u_xlatb10;
float u_xlat20;
vec2 u_xlat24;
bvec2 u_xlatb24;
float u_xlat30;
float u_xlat31;
bool u_xlatb31;
float u_xlat32;
float u_xlat33;
bool u_xlatb33;
float u_xlat34;
void main()
{
    u_xlati0 = gl_InstanceID + unity_BaseInstanceID;
    u_xlati0 = u_xlati0 << 3;
    u_xlat10.xyz = in_POSITION0.yyy * unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz;
    u_xlat10.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz * in_POSITION0.xxx + u_xlat10.xyz;
    u_xlat10.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz * in_POSITION0.zzz + u_xlat10.xyz;
    u_xlat1.xyz = u_xlat10.xyz + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz;
    u_xlat2.x = dot(in_NORMAL0.xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[0].xyz);
    u_xlat2.y = dot(in_NORMAL0.xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[1].xyz);
    u_xlat2.z = dot(in_NORMAL0.xyz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[2].xyz);
    u_xlat31 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat31 = inversesqrt(u_xlat31);
    u_xlat2.xyz = vec3(u_xlat31) * u_xlat2.xyz;
    u_xlat31 = dot(WIND_SETTINGS_WorldDirectionAndSpeed.xyz, WIND_SETTINGS_WorldDirectionAndSpeed.xyz);
    u_xlat31 = inversesqrt(u_xlat31);
    u_xlat3.xyz = vec3(u_xlat31) * WIND_SETTINGS_WorldDirectionAndSpeed.xyz;
    u_xlat4.xy = u_xlat3.xz * WIND_SETTINGS_WorldDirectionAndSpeed.ww;
    u_xlatb31 = 0.0<WIND_SETTINGS_WorldDirectionAndSpeed.w;
    u_xlatb24.xy = lessThan(vec4(0.0, 0.0, 0.0, 0.0), vec4(WIND_SETTINGS_Turbulence, WIND_SETTINGS_GustSpeed, WIND_SETTINGS_Turbulence, WIND_SETTINGS_GustSpeed)).xy;
    u_xlatb31 = u_xlatb31 || u_xlatb24.x;
    if(u_xlatb31){
        u_xlat5.xy = (-u_xlat4.xy) * _Time.yy + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xz;
        u_xlat5.xy = u_xlat5.xy * vec2(WIND_SETTINGS_FlexNoiseScale);
        u_xlat5 = textureLod(WIND_SETTINGS_TexNoise, u_xlat5.xy, 3.0);
        u_xlat5.xyz = u_xlat5.xyz + vec3(-0.5, -0.5, -0.5);
    } else {
        u_xlat5.x = float(0.0);
        u_xlat5.y = float(0.0);
        u_xlat5.z = float(0.0);
    }
    if(u_xlatb24.y){
        u_xlat24.xy = u_xlat3.xz * vec2(vec2(WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed));
        u_xlat24.xy = (-u_xlat24.xy) * _Time.yy + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xz;
        u_xlat24.xy = u_xlat24.xy * vec2(vec2(WIND_SETTINGS_GustWorldScale, WIND_SETTINGS_GustWorldScale));
        u_xlat6 = textureLod(WIND_SETTINGS_TexGust, u_xlat24.xy, 3.0);
        u_xlat31 = u_xlat6.x * u_xlat6.x;
        u_xlat31 = u_xlat31 * WIND_SETTINGS_GustScale;
    } else {
        u_xlat31 = 0.0;
    }
    u_xlat6.xyz = u_xlat3.xyz * vec3(u_xlat31);
    u_xlat6.xyz = u_xlat6.xyz * vec3(vec3(WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed));
    u_xlat3.xyz = u_xlat3.xyz * WIND_SETTINGS_WorldDirectionAndSpeed.www + u_xlat6.xyz;
    u_xlat3.xyz = u_xlat5.xyz * vec3(vec3(WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence)) + u_xlat3.xyz;
    u_xlat5.xyz = u_xlat3.xyz * vec3(vec3(_Drag, _Drag, _Drag));
    u_xlat4.xy = (-u_xlat4.xy) * _Time.yy + u_xlat1.xz;
    u_xlat4.xy = u_xlat4.xy * vec2(vec2(WIND_SETTINGS_ShiverNoiseScale, WIND_SETTINGS_ShiverNoiseScale));
    u_xlat4 = textureLod(WIND_SETTINGS_TexNoise, u_xlat4.xy, 0.0);
    u_xlat4.xyz = u_xlat4.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlat4.xyz = u_xlat4.xyz * vec3(vec3(_ShiverDrag, _ShiverDrag, _ShiverDrag));
    u_xlat4.xyz = u_xlat4.xyz * vec3(vec3(WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence));
    u_xlat32 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat32 = sqrt(u_xlat32);
    u_xlat32 = u_xlat32 + _InitialBend;
    u_xlat33 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat33 = sqrt(u_xlat33);
    u_xlat31 = u_xlat33 * u_xlat31 + u_xlat33;
    u_xlatb33 = 0.0<u_xlat32;
    u_xlat20 = dot(u_xlat10.xyz, u_xlat10.xyz);
    u_xlat20 = sqrt(u_xlat20);
    u_xlat20 = u_xlat20 / _Stiffness;
    u_xlat34 = float(1.0) / _Stiffness;
    u_xlat20 = max(abs(u_xlat20), 1.1920929e-07);
    u_xlat20 = log2(u_xlat20);
    u_xlat20 = u_xlat20 * u_xlat34;
    u_xlat20 = exp2(u_xlat20);
    u_xlat6.xyz = u_xlat5.xyz * vec3(1.0, 0.0, 0.0);
    u_xlat5.xyz = u_xlat5.yzx * vec3(0.0, 1.0, 0.0) + (-u_xlat6.xyz);
    u_xlat20 = u_xlat20 * u_xlat32;
    u_xlat20 = u_xlat20 * 0.00100000005;
    u_xlat32 = dot(u_xlat5.xy, u_xlat5.xy);
    u_xlat32 = inversesqrt(u_xlat32);
    u_xlat5.xyz = vec3(u_xlat32) * u_xlat5.xyz;
    u_xlat10.x = dot(u_xlat5.yx, u_xlat10.xz);
    u_xlat6.xyz = u_xlat5.yzx * u_xlat10.xxx + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz;
    u_xlat7.xyz = u_xlat1.yzx + (-u_xlat6.yzx);
    u_xlat8 = sin(u_xlat20);
    u_xlat9 = cos(u_xlat20);
    u_xlat10.xyz = u_xlat5.xyz * u_xlat7.xyz;
    u_xlat10.xyz = u_xlat5.zxy * u_xlat7.yzx + (-u_xlat10.xyz);
    u_xlat10.xyz = vec3(u_xlat8) * u_xlat10.xyz;
    u_xlat10.xyz = u_xlat7.zxy * vec3(u_xlat9) + u_xlat10.xyz;
    u_xlat10.xyz = u_xlat10.xyz + u_xlat6.xyz;
    u_xlat3.xyz = u_xlat3.xyz * vec3(vec3(_Drag, _Drag, _Drag)) + u_xlat4.xyz;
    u_xlat32 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat32 = inversesqrt(u_xlat32);
    u_xlat3.xyz = u_xlat3.xyz * vec3(u_xlat32) + (-u_xlat2.xyz);
    u_xlat2.xyz = vec3(_ShiverDirectionality) * u_xlat3.xyz + u_xlat2.xyz;
    u_xlat32 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat32 = inversesqrt(u_xlat32);
    u_xlat2.xyz = vec3(u_xlat32) * u_xlat2.xyz;
    u_xlat2.xyz = vec3(u_xlat31) * u_xlat2.xyz;
    u_xlat10.xyz = u_xlat2.xyz * in_COLOR0.www + u_xlat10.xyz;
    u_xlat10.xyz = (bool(u_xlatb33)) ? u_xlat10.xyz : u_xlat1.xyz;
    u_xlat1.xyz = u_xlat10.yyy * unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[1].xyz;
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[0].xyz * u_xlat10.xxx + u_xlat1.xyz;
    u_xlat10.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[2].xyz * u_xlat10.zzz + u_xlat1.xyz;
    u_xlat10.xyz = u_xlat10.xyz + unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[3].xyz;
    vs_TEXCOORD1.xy = in_TEXCOORD0.xy * _texcoord_ST.xy + _texcoord_ST.zw;
    u_xlat1.xyz = u_xlat10.yyy * unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1].xyz;
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0].xyz * u_xlat10.xxx + u_xlat1.xyz;
    u_xlat1.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2].xyz * u_xlat10.zzz + u_xlat1.xyz;
    vs_TEXCOORD2.xyz = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3].xyz * in_POSITION0.www + u_xlat1.xyz;
    u_xlat2 = u_xlat10.yyyy * unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[1];
    u_xlat2 = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[0] * u_xlat10.xxxx + u_xlat2;
    u_xlat2 = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[2] * u_xlat10.zzzz + u_xlat2;
    u_xlat2 = unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_ObjectToWorldArray[3] * in_POSITION0.wwww + u_xlat2;
    u_xlatb10.x = unity_LightShadowBias.z!=0.0;
    if(u_xlatb10.x){
        u_xlatb10.xyz = notEqual(_NewNormal.xyzz, vec4(0.0, 0.0, 0.0, 0.0)).xyz;
        u_xlatb10.x = u_xlatb10.y || u_xlatb10.x;
        u_xlatb10.x = u_xlatb10.z || u_xlatb10.x;
        u_xlat1.xyz = in_NORMAL0.xyz * _NewNormal.xyz;
        u_xlat3.xyz = (u_xlatb10.x) ? u_xlat1.xyz : in_NORMAL0.xyz;
        u_xlatb10.x = _WindNormalInfluence!=0.0;
        u_xlat20 = _WindNormalInfluence + _WindNormalInfluence;
        u_xlat20 = u_xlat31 * u_xlat20 + (-_WindNormalInfluence);
        u_xlat20 = u_xlat20 + u_xlat3.y;
        u_xlat3.w = (u_xlatb10.x) ? u_xlat20 : u_xlat3.y;
        u_xlat1.x = dot(u_xlat3.xwz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[0].xyz);
        u_xlat1.y = dot(u_xlat3.xwz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[1].xyz);
        u_xlat1.z = dot(u_xlat3.xwz, unity_Builtins0Array[u_xlati0 / 8].hlslcc_mtx4x4unity_WorldToObjectArray[2].xyz);
        u_xlat0.x = dot(u_xlat1.xyz, u_xlat1.xyz);
        u_xlat0.x = inversesqrt(u_xlat0.x);
        u_xlat0.xyz = u_xlat0.xxx * u_xlat1.xyz;
        u_xlat1.xyz = (-u_xlat2.xyz) * _WorldSpaceLightPos0.www + _WorldSpaceLightPos0.xyz;
        u_xlat30 = dot(u_xlat1.xyz, u_xlat1.xyz);
        u_xlat30 = inversesqrt(u_xlat30);
        u_xlat1.xyz = vec3(u_xlat30) * u_xlat1.xyz;
        u_xlat30 = dot(u_xlat0.xyz, u_xlat1.xyz);
        u_xlat30 = (-u_xlat30) * u_xlat30 + 1.0;
        u_xlat30 = sqrt(u_xlat30);
        u_xlat30 = u_xlat30 * unity_LightShadowBias.z;
        u_xlat2.xyz = (-u_xlat0.xyz) * vec3(u_xlat30) + u_xlat2.xyz;
    }
    u_xlat0 = u_xlat2.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat2.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat2.zzzz + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat2.wwww + u_xlat0;
    u_xlat1.x = unity_LightShadowBias.x / u_xlat0.w;
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
    u_xlat20 = u_xlat0.z + u_xlat1.x;
    u_xlat1.x = max((-u_xlat0.w), u_xlat20);
    u_xlat1.x = (-u_xlat20) + u_xlat1.x;
    gl_Position.z = unity_LightShadowBias.y * u_xlat1.x + u_xlat20;
    gl_Position.xyw = u_xlat0.xyw;
    vs_SV_InstanceID0 =  uint(gl_InstanceID);
    return;
}

#endif
#ifdef FRAGMENT
#version 330
#extension GL_ARB_explicit_attrib_location : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	float _Cutoff;
UNITY_LOCATION(0) uniform  sampler2D _MainTex;
in  vec2 vs_TEXCOORD1;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
bool u_xlatb0;
void main()
{
    u_xlat0 = texture(_MainTex, vs_TEXCOORD1.xy);
    u_xlat0.x = u_xlat0.w + (-_Cutoff);
    u_xlatb0 = u_xlat0.x<0.0;
    if(((int(u_xlatb0) * int(0xffffffffu)))!=0){discard;}
    SV_Target0 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
                                $Globals         _Cutoff                              $Globals�         _Time                            _WorldSpaceLightPos0                        unity_LightShadowBias                            unity_MatrixVP                    0      unity_BaseInstanceID                 @      _InitialBend                  D   
   _Stiffness                    H      _Drag                     L      _ShiverDrag                   P      _ShiverDirectionality                     T      _WindNormalInfluence                  X   
   _NewNormal                    `   $   WIND_SETTINGS_WorldDirectionAndSpeed                  p      WIND_SETTINGS_FlexNoiseScale                  �      WIND_SETTINGS_ShiverNoiseScale                    �      WIND_SETTINGS_Turbulence                  �      WIND_SETTINGS_GustSpeed                   �      WIND_SETTINGS_GustScale                   �      WIND_SETTINGS_GustWorldScale                  �      _texcoord_ST                  �          UnityInstancing_PerDraw0             unity_Builtins0Array       �         unity_ObjectToWorldArray                        unity_WorldToObjectArray                 @         _MainTex                  WIND_SETTINGS_TexNoise                  WIND_SETTINGS_TexGust                   UnityInstancing_PerDraw0               