�J  <Q                         GPU_FRUSTUM_ON     LOD_FADE_CROSSFADE     SHADOWS_DEPTH      SPOT    �A  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 unity_WorldTransformParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	float _InitialBend;
uniform 	float _Stiffness;
uniform 	float _Drag;
uniform 	vec4 _NewNormal;
uniform 	vec4 WIND_SETTINGS_WorldDirectionAndSpeed;
uniform 	float WIND_SETTINGS_FlexNoiseScale;
uniform 	float WIND_SETTINGS_Turbulence;
uniform 	float WIND_SETTINGS_GustSpeed;
uniform 	float WIND_SETTINGS_GustScale;
uniform 	float WIND_SETTINGS_GustWorldScale;
uniform 	vec4 _texcoord_ST;
UNITY_LOCATION(8) uniform  sampler2D WIND_SETTINGS_TexNoise;
UNITY_LOCATION(9) uniform  sampler2D WIND_SETTINGS_TexGust;
in  vec4 in_POSITION0;
in  vec4 in_TANGENT0;
in  vec3 in_NORMAL0;
in  vec4 in_TEXCOORD0;
out vec2 vs_TEXCOORD0;
out vec3 vs_TEXCOORD1;
out vec3 vs_TEXCOORD2;
out vec3 vs_TEXCOORD3;
out vec3 vs_TEXCOORD4;
out vec4 vs_TEXCOORD5;
out vec4 vs_TEXCOORD6;
vec4 u_xlat0;
vec3 u_xlat1;
bvec3 u_xlatb1;
vec4 u_xlat2;
vec4 u_xlat3;
bvec2 u_xlatb3;
vec4 u_xlat4;
float u_xlat5;
float u_xlat6;
vec3 u_xlat10;
float u_xlat18;
bool u_xlatb18;
float u_xlat19;
bool u_xlatb19;
float u_xlat20;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat18 = dot(WIND_SETTINGS_WorldDirectionAndSpeed.xyz, WIND_SETTINGS_WorldDirectionAndSpeed.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat2.xyz = vec3(u_xlat18) * WIND_SETTINGS_WorldDirectionAndSpeed.xyz;
    u_xlatb18 = 0.0<WIND_SETTINGS_WorldDirectionAndSpeed.w;
    u_xlatb3.xy = lessThan(vec4(0.0, 0.0, 0.0, 0.0), vec4(WIND_SETTINGS_Turbulence, WIND_SETTINGS_GustSpeed, WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence)).xy;
    u_xlatb18 = u_xlatb18 || u_xlatb3.x;
    if(u_xlatb18){
        u_xlat3.xz = u_xlat2.xz * WIND_SETTINGS_WorldDirectionAndSpeed.ww;
        u_xlat3.xz = (-u_xlat3.xz) * _Time.yy + hlslcc_mtx4x4unity_ObjectToWorld[3].xz;
        u_xlat3.xz = u_xlat3.xz * vec2(WIND_SETTINGS_FlexNoiseScale);
        u_xlat4 = textureLod(WIND_SETTINGS_TexNoise, u_xlat3.xz, 3.0);
        u_xlat3.xzw = u_xlat4.xyz + vec3(-0.5, -0.5, -0.5);
    } else {
        u_xlat3.x = float(0.0);
        u_xlat3.z = float(0.0);
        u_xlat3.w = float(0.0);
    }
    if(u_xlatb3.y){
        u_xlat4.xy = u_xlat2.xz * vec2(vec2(WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed));
        u_xlat4.xy = (-u_xlat4.xy) * _Time.yy + hlslcc_mtx4x4unity_ObjectToWorld[3].xz;
        u_xlat4.xy = u_xlat4.xy * vec2(WIND_SETTINGS_GustWorldScale);
        u_xlat4 = textureLod(WIND_SETTINGS_TexGust, u_xlat4.xy, 3.0);
        u_xlat18 = u_xlat4.x * u_xlat4.x;
        u_xlat18 = u_xlat18 * WIND_SETTINGS_GustScale;
    } else {
        u_xlat18 = 0.0;
    }
    u_xlat4.xyz = u_xlat2.xyz * vec3(u_xlat18);
    u_xlat4.xyz = u_xlat4.xyz * vec3(vec3(WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed, WIND_SETTINGS_GustSpeed));
    u_xlat2.xyz = u_xlat2.xyz * WIND_SETTINGS_WorldDirectionAndSpeed.www + u_xlat4.xyz;
    u_xlat2.xyz = u_xlat3.xzw * vec3(vec3(WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence, WIND_SETTINGS_Turbulence)) + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz * vec3(vec3(_Drag, _Drag, _Drag));
    u_xlat18 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat18 = sqrt(u_xlat18);
    u_xlat18 = u_xlat18 + _InitialBend;
    u_xlatb19 = 0.0<u_xlat18;
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = sqrt(u_xlat6);
    u_xlat6 = u_xlat6 / _Stiffness;
    u_xlat20 = float(1.0) / _Stiffness;
    u_xlat6 = max(abs(u_xlat6), 1.1920929e-07);
    u_xlat6 = log2(u_xlat6);
    u_xlat6 = u_xlat6 * u_xlat20;
    u_xlat6 = exp2(u_xlat6);
    u_xlat3.xyz = u_xlat2.xyz * vec3(1.0, 0.0, 0.0);
    u_xlat2.xyz = u_xlat2.yzx * vec3(0.0, 1.0, 0.0) + (-u_xlat3.xyz);
    u_xlat6 = u_xlat6 * u_xlat18;
    u_xlat6 = u_xlat6 * 0.00100000005;
    u_xlat18 = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat2.xyz = vec3(u_xlat18) * u_xlat2.xyz;
    u_xlat0.x = dot(u_xlat2.yx, u_xlat0.xz);
    u_xlat0.xzw = u_xlat2.yzx * u_xlat0.xxx + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat3.xyz = (-u_xlat0.zwx) + u_xlat1.yzx;
    u_xlat4.x = sin(u_xlat6);
    u_xlat5 = cos(u_xlat6);
    u_xlat10.xyz = u_xlat2.xyz * u_xlat3.xyz;
    u_xlat2.xyz = u_xlat2.zxy * u_xlat3.yzx + (-u_xlat10.xyz);
    u_xlat2.xyz = u_xlat4.xxx * u_xlat2.xyz;
    u_xlat2.xyz = u_xlat3.zxy * vec3(u_xlat5) + u_xlat2.xyz;
    u_xlat0.xyz = u_xlat0.xzw + u_xlat2.xyz;
    u_xlat0.xyz = (bool(u_xlatb19)) ? u_xlat0.xyz : u_xlat1.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_WorldToObject[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlatb1.xyz = notEqual(_NewNormal.xyzx, vec4(0.0, 0.0, 0.0, 0.0)).xyz;
    u_xlatb18 = u_xlatb1.y && u_xlatb1.x;
    u_xlatb18 = u_xlatb1.z && u_xlatb18;
    u_xlat1.xyz = in_NORMAL0.xyz * _NewNormal.xyz;
    u_xlat1.xyz = (bool(u_xlatb18)) ? u_xlat1.xyz : in_NORMAL0.xyz;
    u_xlat2 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat2 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat2;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat2;
    u_xlat2 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat3 = u_xlat2.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat2.xxxx + u_xlat3;
    u_xlat3 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat2.zzzz + u_xlat3;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat2.wwww + u_xlat3;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _texcoord_ST.xy + _texcoord_ST.zw;
    vs_TEXCOORD4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat2.y = dot(u_xlat1.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat2.z = dot(u_xlat1.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat2.x = dot(u_xlat1.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat1.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat1.xyz = u_xlat1.xxx * u_xlat2.xyz;
    u_xlat2.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].yzx;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].yzx * in_TANGENT0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].yzx * in_TANGENT0.zzz + u_xlat2.xyz;
    u_xlat19 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat19 = inversesqrt(u_xlat19);
    u_xlat2.xyz = vec3(u_xlat19) * u_xlat2.xyz;
    u_xlat19 = in_TANGENT0.w * unity_WorldTransformParams.w;
    u_xlat3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat3.xyz = u_xlat1.zxy * u_xlat2.yzx + (-u_xlat3.xyz);
    u_xlat3.xyz = vec3(u_xlat19) * u_xlat3.xyz;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat4 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat4 = hlslcc_mtx4x4unity_WorldToLight[0] * u_xlat0.xxxx + u_xlat4;
    u_xlat4 = hlslcc_mtx4x4unity_WorldToLight[2] * u_xlat0.zzzz + u_xlat4;
    vs_TEXCOORD5 = hlslcc_mtx4x4unity_WorldToLight[3] * u_xlat0.wwww + u_xlat4;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD1.x = u_xlat2.z;
    vs_TEXCOORD1.y = u_xlat3.x;
    vs_TEXCOORD1.z = u_xlat1.y;
    vs_TEXCOORD2.x = u_xlat2.x;
    vs_TEXCOORD2.y = u_xlat3.y;
    vs_TEXCOORD2.z = u_xlat1.z;
    vs_TEXCOORD3.x = u_xlat2.y;
    vs_TEXCOORD3.y = u_xlat3.z;
    vs_TEXCOORD3.z = u_xlat1.x;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 unity_OcclusionMaskSelector;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 unity_LODFade;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 unity_ProbeVolumeParams;
uniform 	vec4 hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[4];
uniform 	vec3 unity_ProbeVolumeSizeInv;
uniform 	vec3 unity_ProbeVolumeMin;
uniform 	vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	float _BumpScale;
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _HealthyColor;
uniform 	vec3 _ColorAdjustment;
uniform 	float _Smooothness;
uniform 	float _Cutoff;
UNITY_LOCATION(0) uniform  sampler2D _BumpMap;
UNITY_LOCATION(1) uniform  sampler2D _MainTex;
UNITY_LOCATION(2) uniform  sampler2D unity_DitherMask;
UNITY_LOCATION(3) uniform  sampler2D _LightTexture0;
UNITY_LOCATION(4) uniform  sampler2D _LightTextureB0;
UNITY_LOCATION(5) uniform  sampler3D unity_ProbeVolumeSH;
UNITY_LOCATION(6) uniform  sampler2D _ShadowMapTexture;
UNITY_LOCATION(7) uniform  sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
in  vec2 vs_TEXCOORD0;
in  vec3 vs_TEXCOORD1;
in  vec3 vs_TEXCOORD2;
in  vec3 vs_TEXCOORD3;
in  vec3 vs_TEXCOORD4;
layout(location = 0) out vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat2;
bool u_xlatb2;
vec3 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
float u_xlat8;
vec3 u_xlat10;
float u_xlat16;
float u_xlat18;
vec2 u_xlat20;
float u_xlat24;
float u_xlat25;
bool u_xlatb25;
float u_xlat26;
float u_xlat27;
void main()
{
vec4 hlslcc_FragCoord = vec4(gl_FragCoord.xyz, 1.0/gl_FragCoord.w);
    u_xlat0.xyz = (-vs_TEXCOORD4.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat1.xyz = vec3(u_xlat24) * u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD4.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat25 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat25 = inversesqrt(u_xlat25);
    u_xlat3.xyz = vec3(u_xlat25) * u_xlat2.xyz;
    u_xlat4.xy = vs_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat5 = texture(_BumpMap, u_xlat4.xy);
    u_xlat5.x = u_xlat5.w * u_xlat5.x;
    u_xlat20.xy = u_xlat5.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat5.xy = u_xlat20.xy * vec2(vec2(_BumpScale, _BumpScale));
    u_xlat25 = dot(u_xlat5.xy, u_xlat5.xy);
    u_xlat25 = min(u_xlat25, 1.0);
    u_xlat25 = (-u_xlat25) + 1.0;
    u_xlat5.z = sqrt(u_xlat25);
    u_xlat4 = texture(_MainTex, u_xlat4.xy);
    u_xlat4.xyz = u_xlat4.xyz * _HealthyColor.xyz;
    u_xlat4.xyz = u_xlat4.xyz * _ColorAdjustment.xyz;
    u_xlat25 = u_xlat4.w + (-_Cutoff);
    u_xlatb25 = u_xlat25<0.0;
    if(((int(u_xlatb25) * int(0xffffffffu)))!=0){discard;}
    u_xlat6.xy = hlslcc_FragCoord.xy * vec2(0.25, 0.25);
    u_xlat6 = texture(unity_DitherMask, u_xlat6.xy);
    u_xlatb25 = 0.0<unity_LODFade.x;
    u_xlat25 = (u_xlatb25) ? 1.0 : -1.0;
    u_xlat25 = (-u_xlat6.w) * u_xlat25 + unity_LODFade.x;
    u_xlatb25 = u_xlat25<0.0;
    if(((int(u_xlatb25) * int(0xffffffffu)))!=0){discard;}
    u_xlat6 = vs_TEXCOORD4.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat6 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD4.xxxx + u_xlat6;
    u_xlat6 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD4.zzzz + u_xlat6;
    u_xlat6 = u_xlat6 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat7.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat7.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat7.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat25 = dot(u_xlat2.xyz, u_xlat7.xyz);
    u_xlat2.xyz = vs_TEXCOORD4.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat2.x = sqrt(u_xlat2.x);
    u_xlat2.x = (-u_xlat25) + u_xlat2.x;
    u_xlat25 = unity_ShadowFadeCenterAndType.w * u_xlat2.x + u_xlat25;
    u_xlat25 = u_xlat25 * _LightShadowData.z + _LightShadowData.w;
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
    u_xlatb2 = unity_ProbeVolumeParams.x==1.0;
    if(u_xlatb2){
        u_xlatb2 = unity_ProbeVolumeParams.y==1.0;
        u_xlat10.xyz = vs_TEXCOORD4.yyy * hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[1].xyz;
        u_xlat10.xyz = hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[0].xyz * vs_TEXCOORD4.xxx + u_xlat10.xyz;
        u_xlat10.xyz = hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[2].xyz * vs_TEXCOORD4.zzz + u_xlat10.xyz;
        u_xlat10.xyz = u_xlat10.xyz + hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[3].xyz;
        u_xlat2.xyz = (bool(u_xlatb2)) ? u_xlat10.xyz : vs_TEXCOORD4.xyz;
        u_xlat2.xyz = u_xlat2.xyz + (-unity_ProbeVolumeMin.xyz);
        u_xlat2.yzw = u_xlat2.xyz * unity_ProbeVolumeSizeInv.xyz;
        u_xlat10.x = u_xlat2.y * 0.25 + 0.75;
        u_xlat27 = unity_ProbeVolumeParams.z * 0.5 + 0.75;
        u_xlat2.x = max(u_xlat10.x, u_xlat27);
        u_xlat2 = texture(unity_ProbeVolumeSH, u_xlat2.xzw);
    } else {
        u_xlat2.x = float(1.0);
        u_xlat2.y = float(1.0);
        u_xlat2.z = float(1.0);
        u_xlat2.w = float(1.0);
    }
    u_xlat2.x = dot(u_xlat2, unity_OcclusionMaskSelector);
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
    u_xlat7 = vs_TEXCOORD4.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat7 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD4.xxxx + u_xlat7;
    u_xlat7 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD4.zzzz + u_xlat7;
    u_xlat7 = u_xlat7 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat10.xyz = u_xlat7.xyz / u_xlat7.www;
    vec3 txVec0 = vec3(u_xlat10.xy,u_xlat10.z);
    u_xlat10.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat18 = (-_LightShadowData.x) + 1.0;
    u_xlat10.x = u_xlat10.x * u_xlat18 + _LightShadowData.x;
    u_xlat2.x = (-u_xlat10.x) + u_xlat2.x;
    u_xlat25 = u_xlat25 * u_xlat2.x + u_xlat10.x;
    u_xlatb2 = 0.0<u_xlat6.z;
    u_xlat2.x = u_xlatb2 ? 1.0 : float(0.0);
    u_xlat10.xy = u_xlat6.xy / u_xlat6.ww;
    u_xlat10.xy = u_xlat10.xy + vec2(0.5, 0.5);
    u_xlat7 = texture(_LightTexture0, u_xlat10.xy);
    u_xlat2.x = u_xlat2.x * u_xlat7.w;
    u_xlat10.x = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat6 = texture(_LightTextureB0, u_xlat10.xx);
    u_xlat2.x = u_xlat2.x * u_xlat6.x;
    u_xlat25 = u_xlat25 * u_xlat2.x;
    u_xlat2.x = dot(vs_TEXCOORD1.xyz, u_xlat5.xyz);
    u_xlat2.y = dot(vs_TEXCOORD2.xyz, u_xlat5.xyz);
    u_xlat2.z = dot(vs_TEXCOORD3.xyz, u_xlat5.xyz);
    u_xlat26 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat26 = inversesqrt(u_xlat26);
    u_xlat2.xyz = vec3(u_xlat26) * u_xlat2.xyz;
    u_xlat5.xyz = vec3(u_xlat25) * _LightColor0.xyz;
    u_xlat25 = (-_Smooothness) + 1.0;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat24) + u_xlat3.xyz;
    u_xlat24 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat24 = max(u_xlat24, 0.00100000005);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat0.xyz = vec3(u_xlat24) * u_xlat0.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat3.xyz);
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat1.xyz);
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = dot(u_xlat0.xx, vec2(u_xlat25));
    u_xlat0.x = u_xlat0.x + -0.5;
    u_xlat8 = (-u_xlat2.x) + 1.0;
    u_xlat16 = u_xlat8 * u_xlat8;
    u_xlat16 = u_xlat16 * u_xlat16;
    u_xlat8 = u_xlat8 * u_xlat16;
    u_xlat8 = u_xlat0.x * u_xlat8 + 1.0;
    u_xlat16 = -abs(u_xlat24) + 1.0;
    u_xlat24 = u_xlat16 * u_xlat16;
    u_xlat24 = u_xlat24 * u_xlat24;
    u_xlat16 = u_xlat16 * u_xlat24;
    u_xlat0.x = u_xlat0.x * u_xlat16 + 1.0;
    u_xlat0.x = u_xlat0.x * u_xlat8;
    u_xlat0.x = u_xlat2.x * u_xlat0.x;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat5.xyz;
    SV_Target0.xyz = u_xlat0.xyz * u_xlat4.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
                                $GlobalsT        _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        unity_OcclusionMaskSelector                          unity_WorldToShadow                  0      _LightShadowData                  p      unity_ShadowFadeCenterAndType                     �      unity_LODFade                     �      unity_MatrixV                     �      unity_ProbeVolumeParams                   �      unity_ProbeVolumeWorldToObject                    �      unity_ProbeVolumeSizeInv                  �      unity_ProbeVolumeMin                  �      _LightColor0                  �      unity_WorldToLight                       
   _BumpScale                         _MainTex_ST                         _HealthyColor                     0     _ColorAdjustment                  @     _Smooothness                  L     _Cutoff                   P         $Globals�         _Time                            unity_ObjectToWorld                         unity_WorldToObject                          unity_WorldTransformParams                    0      unity_MatrixVP                    @      unity_WorldToLight                    P      _InitialBend                  `   
   _Stiffness                    d      _Drag                     h   
   _NewNormal                    p   $   WIND_SETTINGS_WorldDirectionAndSpeed                  �      WIND_SETTINGS_FlexNoiseScale                  �      WIND_SETTINGS_Turbulence                  �      WIND_SETTINGS_GustSpeed                   �      WIND_SETTINGS_GustScale                   �      WIND_SETTINGS_GustWorldScale                  �      _texcoord_ST                  �       	      _BumpMap                  _MainTex                unity_DitherMask                _LightTexture0                  _LightTextureB0                 unity_ProbeVolumeSH                 _ShadowMapTexture                   WIND_SETTINGS_TexNoise                  WIND_SETTINGS_TexGust       	   	          