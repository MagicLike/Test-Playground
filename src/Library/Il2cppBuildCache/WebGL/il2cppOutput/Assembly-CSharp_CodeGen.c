﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void vThirdPersonCamera::Start()
extern void vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454 (void);
// 0x00000002 System.Void vThirdPersonCamera::Init()
extern void vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6 (void);
// 0x00000003 System.Void vThirdPersonCamera::FixedUpdate()
extern void vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6 (void);
// 0x00000004 System.Void vThirdPersonCamera::SetTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7 (void);
// 0x00000005 System.Void vThirdPersonCamera::SetMainTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7 (void);
// 0x00000006 UnityEngine.Ray vThirdPersonCamera::ScreenPointToRay(UnityEngine.Vector3)
extern void vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144 (void);
// 0x00000007 System.Void vThirdPersonCamera::RotateCamera(System.Single,System.Single)
extern void vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57 (void);
// 0x00000008 System.Void vThirdPersonCamera::CameraMovement()
extern void vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2 (void);
// 0x00000009 System.Boolean vThirdPersonCamera::CullingRayCast(UnityEngine.Vector3,Invector.ClipPlanePoints,UnityEngine.RaycastHit&,System.Single,UnityEngine.LayerMask,UnityEngine.Color)
extern void vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2 (void);
// 0x0000000A System.Void vThirdPersonCamera::.ctor()
extern void vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6 (void);
// 0x0000000B System.Void vPickupItem::Start()
extern void vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070 (void);
// 0x0000000C System.Void vPickupItem::OnTriggerEnter(UnityEngine.Collider)
extern void vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4 (void);
// 0x0000000D System.Void vPickupItem::.ctor()
extern void vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F (void);
// 0x0000000E System.Void NM_Wind::Start()
extern void NM_Wind_Start_m5AD8914FCA79D5988FC56B7BAD1C6E1BABCF4A88 (void);
// 0x0000000F System.Void NM_Wind::Update()
extern void NM_Wind_Update_m667D6826096516C86964269423A7A22EEEC8D164 (void);
// 0x00000010 System.Void NM_Wind::OnValidate()
extern void NM_Wind_OnValidate_m34684340FF474FE412309030FBA8FAAF70DC65CC (void);
// 0x00000011 System.Void NM_Wind::ApplySettings()
extern void NM_Wind_ApplySettings_mA0F98E67AC89B7E1D60348A382244B272C4DBA17 (void);
// 0x00000012 UnityEngine.Vector4 NM_Wind::GetDirectionAndSpeed()
extern void NM_Wind_GetDirectionAndSpeed_m03651A4FEA264BAB2EA453BBCA128D0F4AB00C02 (void);
// 0x00000013 System.Void NM_Wind::.ctor()
extern void NM_Wind__ctor_mB36F8772CEBFF30CF9732C1A1B245449F7470264 (void);
// 0x00000014 System.Void RotateGameObject::Start()
extern void RotateGameObject_Start_m86E4C1345156019C6DD3344CFBC593502CCF7BA1 (void);
// 0x00000015 System.Void RotateGameObject::FixedUpdate()
extern void RotateGameObject_FixedUpdate_m74844D804AED248B8F292C3876F0A0ABDA818FF1 (void);
// 0x00000016 System.Void RotateGameObject::.ctor()
extern void RotateGameObject__ctor_m1E3B1D5EF4FD3337D946B6AE7CCACC5DC66B2735 (void);
// 0x00000017 System.Void FallDeath::OnTriggerEnter(UnityEngine.Collider)
extern void FallDeath_OnTriggerEnter_m7FB4AF593637C636B6A356DEFDF376E34C253044 (void);
// 0x00000018 System.Void FallDeath::.ctor()
extern void FallDeath__ctor_m3B9B4602FB44CC1B0B5FD95B9F6DA27DB37373BA (void);
// 0x00000019 System.Void Controller::Start()
extern void Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157 (void);
// 0x0000001A System.Void Controller::timerStart()
extern void Controller_timerStart_m8FC16BAFC6C835CB57CCF74DFD09D3A4E1C11474 (void);
// 0x0000001B System.Void Controller::won()
extern void Controller_won_mFCB56D38EE427E6C8A0B9B0A0B1B44559208D9C9 (void);
// 0x0000001C System.Void Controller::MainMenu()
extern void Controller_MainMenu_m8B8DB6DD378C27822D96DED01FAC70530B371BFA (void);
// 0x0000001D System.Void Controller::Restart()
extern void Controller_Restart_m8C4712D53B64537760E77F6376504A12D3ACE414 (void);
// 0x0000001E System.Void Controller::pause()
extern void Controller_pause_mE0DA7BCC01AC8F771748B14266E573DE187A95E7 (void);
// 0x0000001F System.Void Controller::resume()
extern void Controller_resume_m580CC5F549CCCD80C950D5028B70E8E6BAF21D06 (void);
// 0x00000020 System.Void Controller::CheckpointReset()
extern void Controller_CheckpointReset_mC3D11518DB641B97E8084485E5B3A10F3A30E6AC (void);
// 0x00000021 System.Void Controller::Update()
extern void Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0 (void);
// 0x00000022 System.String Controller::FormatTime(System.Single)
extern void Controller_FormatTime_m0D417D2D91E0A57A7558750CF44E9CC3A2E90776 (void);
// 0x00000023 System.Void Controller::.ctor()
extern void Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7 (void);
// 0x00000024 System.Void WonScreenTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void WonScreenTrigger_OnTriggerEnter_mA119AD5F27BF9BBE93F2F40AD0FF0C2969451CA8 (void);
// 0x00000025 System.Void WonScreenTrigger::.ctor()
extern void WonScreenTrigger__ctor_m4559CE0ADA19F2F9FD1A6AD9DD9181A6C858AA84 (void);
// 0x00000026 System.Void Goal::ActivateGameObject()
extern void Goal_ActivateGameObject_m0963B47EA91627A061C952581F2D155DD6C77E58 (void);
// 0x00000027 System.Collections.IEnumerator Goal::turnOffAgain()
extern void Goal_turnOffAgain_m5BB614848C4F83F0208B481072625A8E59853F72 (void);
// 0x00000028 System.Void Goal::OnTriggerEnter(UnityEngine.Collider)
extern void Goal_OnTriggerEnter_m1E8C1505F29010BEE13A9710AFDC79D08C9AF671 (void);
// 0x00000029 System.Void Goal::.ctor()
extern void Goal__ctor_m514574E513A17F8F4815CDC89A625EA405039239 (void);
// 0x0000002A System.Void Goal/<turnOffAgain>d__2::.ctor(System.Int32)
extern void U3CturnOffAgainU3Ed__2__ctor_m32CB265E1C0A1BBE36E815446BF016B20485AA06 (void);
// 0x0000002B System.Void Goal/<turnOffAgain>d__2::System.IDisposable.Dispose()
extern void U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m62376BA17CD17D61F28ECF78D1C9795943A024CC (void);
// 0x0000002C System.Boolean Goal/<turnOffAgain>d__2::MoveNext()
extern void U3CturnOffAgainU3Ed__2_MoveNext_mD79EF42793AEB1CEB7E6D01793A66D19AEE3816C (void);
// 0x0000002D System.Object Goal/<turnOffAgain>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC252585D779D8FA7B7181F8351C1D41AD060A7D9 (void);
// 0x0000002E System.Void Goal/<turnOffAgain>d__2::System.Collections.IEnumerator.Reset()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_m75CEF9472B580421D306233C4CCFFD4A39EF847C (void);
// 0x0000002F System.Object Goal/<turnOffAgain>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_mEC0F6C089E8328E215800F30615664394B8C1674 (void);
// 0x00000030 System.Void Level2Teleport::SwitchScene()
extern void Level2Teleport_SwitchScene_m708C82060005CB82C476EF03535B809E1AEADAAB (void);
// 0x00000031 System.Void Level2Teleport::OnTriggerEnter(UnityEngine.Collider)
extern void Level2Teleport_OnTriggerEnter_mB76C5EFA2EDBC88C5B45BACCD6AD1CFF1D578AA6 (void);
// 0x00000032 System.Void Level2Teleport::.ctor()
extern void Level2Teleport__ctor_mFB32379590C6C963B4C2757D11E70896A60A2252 (void);
// 0x00000033 System.Void Lift::OnTriggerEnter(UnityEngine.Collider)
extern void Lift_OnTriggerEnter_mE790F569FEB64383D3C27CA7CBECBA38DAB39BC6 (void);
// 0x00000034 System.Void Lift::OnTriggerExit(UnityEngine.Collider)
extern void Lift_OnTriggerExit_m57E09B2F00AB87CC74093FF18FC9D7D8E95CFE5E (void);
// 0x00000035 System.Void Lift::.ctor()
extern void Lift__ctor_m0FBFBCF032FA5111604D4D7193C5E70D07754E01 (void);
// 0x00000036 System.Void Title::Start()
extern void Title_Start_m31B7E4E0F67327EE18202AD00430FB7000A526ED (void);
// 0x00000037 System.Collections.IEnumerator Title::turnOffAgain()
extern void Title_turnOffAgain_mB96F1A75FC6BDEDD17616E23DF8FC8C2D5003833 (void);
// 0x00000038 System.Void Title::.ctor()
extern void Title__ctor_mF2CBA7A07A8BDFAF5453BC101694B429F9A5F7D1 (void);
// 0x00000039 System.Void Title/<turnOffAgain>d__2::.ctor(System.Int32)
extern void U3CturnOffAgainU3Ed__2__ctor_m374C026397F29CC566F0B8DA9537463FA1892D35 (void);
// 0x0000003A System.Void Title/<turnOffAgain>d__2::System.IDisposable.Dispose()
extern void U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m69CE49C38783CCBCE7B601C40D57750C06BD700B (void);
// 0x0000003B System.Boolean Title/<turnOffAgain>d__2::MoveNext()
extern void U3CturnOffAgainU3Ed__2_MoveNext_mBE215163D7C39BD10602A4071FE65411752AC3E4 (void);
// 0x0000003C System.Object Title/<turnOffAgain>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE19ECE269C4F22788B995A16F85AB1D49756146 (void);
// 0x0000003D System.Void Title/<turnOffAgain>d__2::System.Collections.IEnumerator.Reset()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_m7BE9F4C59A51008962B0EFB0FA7A544159DE3267 (void);
// 0x0000003E System.Object Title/<turnOffAgain>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_m5EB8A43D4E5E9C35C448CC6FF516070E0719FD7B (void);
// 0x0000003F System.Void CheckpointSave::Start()
extern void CheckpointSave_Start_m4562257ABF964E696BA98A7270EE3DD118450BE8 (void);
// 0x00000040 System.Void CheckpointSave::saveNew(UnityEngine.Vector3)
extern void CheckpointSave_saveNew_m8D3F1694990623CCE10E743B206C3C42C142F669 (void);
// 0x00000041 System.Void CheckpointSave::Reset()
extern void CheckpointSave_Reset_mF243B781338FB4314316199762D4B5CB62129D09 (void);
// 0x00000042 System.Void CheckpointSave::.ctor()
extern void CheckpointSave__ctor_m2CD777C615911A86F52DD3EEA8A2FEB1D58630B6 (void);
// 0x00000043 System.Void CheckpointTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void CheckpointTrigger_OnTriggerEnter_m90A6BA7B9E420368E50AF84B86F71E0AEB24CF76 (void);
// 0x00000044 System.Void CheckpointTrigger::.ctor()
extern void CheckpointTrigger__ctor_mC50DCFF72998CD137E36196B130A6821C1E6ABFF (void);
// 0x00000045 System.Void FlyingEnemies::OnTriggerEnter(UnityEngine.Collider)
extern void FlyingEnemies_OnTriggerEnter_m4F85ABDD204E193798D2AA75713FA8FD34099440 (void);
// 0x00000046 System.Void FlyingEnemies::.ctor()
extern void FlyingEnemies__ctor_m1F98CE74FBA4C059E1754066031C6B928E893509 (void);
// 0x00000047 System.Void Title2::Start()
extern void Title2_Start_m1BA01B9BF0E3685F6548A8370525F6703CBF0DFA (void);
// 0x00000048 System.Collections.IEnumerator Title2::turnOffAgain()
extern void Title2_turnOffAgain_mDD8D5B4DB6E71E2C3101DAAD52F34E4929B0BF8E (void);
// 0x00000049 System.Void Title2::.ctor()
extern void Title2__ctor_m73D6720BD9BA5CFCF3E723904D0176A024FDFD57 (void);
// 0x0000004A System.Void Title2/<turnOffAgain>d__2::.ctor(System.Int32)
extern void U3CturnOffAgainU3Ed__2__ctor_mF695D8FC80AEEADE2207887350ED99BF20E82AFC (void);
// 0x0000004B System.Void Title2/<turnOffAgain>d__2::System.IDisposable.Dispose()
extern void U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m943EC455D02E031857134967F6327F4B78F9A1C9 (void);
// 0x0000004C System.Boolean Title2/<turnOffAgain>d__2::MoveNext()
extern void U3CturnOffAgainU3Ed__2_MoveNext_m4FD12C5D49634FD95ACD0A2DB1F57F607CC1C784 (void);
// 0x0000004D System.Object Title2/<turnOffAgain>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD56B68D16C56A39059F342879F2E8C197B942D89 (void);
// 0x0000004E System.Void Title2/<turnOffAgain>d__2::System.Collections.IEnumerator.Reset()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_mB8D1E8F46164E4FBDC13093A9FE31963C45C71AA (void);
// 0x0000004F System.Object Title2/<turnOffAgain>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_mE97EA85905F53D8519416505980BD13CF173CB79 (void);
// 0x00000050 System.Void MainMenu::SwitchScene()
extern void MainMenu_SwitchScene_mA606486AA43867C8F874C56251EC9FD109428C1D (void);
// 0x00000051 System.Void MainMenu::CloseGame()
extern void MainMenu_CloseGame_m595F4E0AC6AF40ABAF8A5466A7931B799C45259F (void);
// 0x00000052 System.Void MainMenu::HowToPlay()
extern void MainMenu_HowToPlay_m0DCA1FF77E60CBEBAB42B1D2F548F9CD908B512E (void);
// 0x00000053 System.Void MainMenu::HowToPlayBack()
extern void MainMenu_HowToPlayBack_mEC7F54C3A215F61BB40A4B5EFE634170E002F172 (void);
// 0x00000054 System.Void MainMenu::Settings()
extern void MainMenu_Settings_m2CA6225F651EF0A8B1FAB410C7158654D548B5DA (void);
// 0x00000055 System.Void MainMenu::SettingsBack()
extern void MainMenu_SettingsBack_mC4A92A9091DB48A3DD5BD7AD9DFEE557347C61B6 (void);
// 0x00000056 System.Void MainMenu::About()
extern void MainMenu_About_m14A470171CAB4DA6AF1D39168D15635658FA5B0F (void);
// 0x00000057 System.Void MainMenu::AboutBack()
extern void MainMenu_AboutBack_m88DB6F8B446CDA924501C96FCC0F00236D54E70C (void);
// 0x00000058 System.Void MainMenu::Levels()
extern void MainMenu_Levels_mE65AB9B148AFBE616CFF9F5FC7F1D00A1AB1ED6C (void);
// 0x00000059 System.Void MainMenu::LevelsBack()
extern void MainMenu_LevelsBack_m4FA837F4DCE46BE4A99D52CC704C706CB3E2481F (void);
// 0x0000005A System.Void MainMenu::GitHub()
extern void MainMenu_GitHub_m905535F114753813DF6EDD2C9F541195168EA4B8 (void);
// 0x0000005B System.Void MainMenu::GitLab()
extern void MainMenu_GitLab_mCDFE6F3CEDE5BCFC7D3DE1C2D8CEABE8583C4E92 (void);
// 0x0000005C System.Void MainMenu::itch()
extern void MainMenu_itch_m019DACDB991A35F957F91CC8171F80A78FEB932D (void);
// 0x0000005D System.Void MainMenu::RickRoll()
extern void MainMenu_RickRoll_mAC7939B6A257D04543BC1AB775CEDA4995DC4157 (void);
// 0x0000005E System.Void MainMenu::Jebaited()
extern void MainMenu_Jebaited_m5FD1F7B71C3F9E9BA67CC9BE68F58020CFB810DF (void);
// 0x0000005F System.Void MainMenu::Level1()
extern void MainMenu_Level1_m98D042A3C61CBD8F9A9370DDEC06CE247D4106F1 (void);
// 0x00000060 System.Void MainMenu::Level2()
extern void MainMenu_Level2_m72C6F165FAC5D290F0AE152BA1C2B561C267B959 (void);
// 0x00000061 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000062 System.Void LowPolyWater.LowPolyWater::Awake()
extern void LowPolyWater_Awake_mBF6D32C683E8BD39148DB6E7AB59FA73EAB6B1C9 (void);
// 0x00000063 System.Void LowPolyWater.LowPolyWater::Start()
extern void LowPolyWater_Start_m89AFA69FF7508A8D612A6C5E5DACB95F2B94AC5B (void);
// 0x00000064 UnityEngine.MeshFilter LowPolyWater.LowPolyWater::CreateMeshLowPoly(UnityEngine.MeshFilter)
extern void LowPolyWater_CreateMeshLowPoly_mE6F5928759402E4367E29A84DB1B4C8FFAAE63D0 (void);
// 0x00000065 System.Void LowPolyWater.LowPolyWater::Update()
extern void LowPolyWater_Update_m5E2FF7C7587E28FA08EA29B9BDD359F190156CA9 (void);
// 0x00000066 System.Void LowPolyWater.LowPolyWater::GenerateWaves()
extern void LowPolyWater_GenerateWaves_m128D40285442A523C43AAB1BA22EF0E21544CEEE (void);
// 0x00000067 System.Void LowPolyWater.LowPolyWater::.ctor()
extern void LowPolyWater__ctor_m616A0E96E64BD42A2ECBAB3FE69EE017057502DE (void);
// 0x00000068 System.Void Invector.vAnimateUV::Update()
extern void vAnimateUV_Update_mFBBF4618D6C5A03EA338C7F81A15BD54F92E5C6B (void);
// 0x00000069 System.Void Invector.vAnimateUV::.ctor()
extern void vAnimateUV__ctor_m5DA31CAF76CFC8D3439AA7AA3DCAA989EDE06C3F (void);
// 0x0000006A T[] Invector.vExtensions::Append(T[],T[])
// 0x0000006B T[] Invector.vExtensions::vToArray(System.Collections.Generic.List`1<T>)
// 0x0000006C System.Single Invector.vExtensions::ClampAngle(System.Single,System.Single,System.Single)
extern void vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B (void);
// 0x0000006D Invector.ClipPlanePoints Invector.vExtensions::NearClipPlanePoints(UnityEngine.Camera,UnityEngine.Vector3,System.Single)
extern void vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772 (void);
// 0x0000006E System.Void Invector.Utils.vComment::.ctor()
extern void vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF (void);
// 0x0000006F System.Void Invector.vCharacterController.vThirdPersonAnimator::UpdateAnimator()
extern void vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E (void);
// 0x00000070 System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02 (void);
// 0x00000071 System.Void Invector.vCharacterController.vThirdPersonAnimator::.ctor()
extern void vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A (void);
// 0x00000072 System.Void Invector.vCharacterController.vAnimatorParameters::.cctor()
extern void vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B (void);
// 0x00000073 System.Void Invector.vCharacterController.vThirdPersonController::ControlAnimatorRootMotion()
extern void vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D (void);
// 0x00000074 System.Void Invector.vCharacterController.vThirdPersonController::ControlLocomotionType()
extern void vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36 (void);
// 0x00000075 System.Void Invector.vCharacterController.vThirdPersonController::ControlRotationType()
extern void vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A (void);
// 0x00000076 System.Void Invector.vCharacterController.vThirdPersonController::UpdateMoveDirection(UnityEngine.Transform)
extern void vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7 (void);
// 0x00000077 System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean)
extern void vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC (void);
// 0x00000078 System.Void Invector.vCharacterController.vThirdPersonController::Strafe()
extern void vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189 (void);
// 0x00000079 System.Void Invector.vCharacterController.vThirdPersonController::Jump()
extern void vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8 (void);
// 0x0000007A System.Void Invector.vCharacterController.vThirdPersonController::.ctor()
extern void vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164 (void);
// 0x0000007B System.Void Invector.vCharacterController.vThirdPersonInput::Start()
extern void vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080 (void);
// 0x0000007C System.Void Invector.vCharacterController.vThirdPersonInput::FixedUpdate()
extern void vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33 (void);
// 0x0000007D System.Void Invector.vCharacterController.vThirdPersonInput::Update()
extern void vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633 (void);
// 0x0000007E System.Void Invector.vCharacterController.vThirdPersonInput::OnAnimatorMove()
extern void vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117 (void);
// 0x0000007F System.Void Invector.vCharacterController.vThirdPersonInput::InitilizeController()
extern void vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1 (void);
// 0x00000080 System.Void Invector.vCharacterController.vThirdPersonInput::InitializeTpCamera()
extern void vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8 (void);
// 0x00000081 System.Void Invector.vCharacterController.vThirdPersonInput::InputHandle()
extern void vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875 (void);
// 0x00000082 System.Void Invector.vCharacterController.vThirdPersonInput::MoveInput()
extern void vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830 (void);
// 0x00000083 System.Void Invector.vCharacterController.vThirdPersonInput::CameraInput()
extern void vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576 (void);
// 0x00000084 System.Void Invector.vCharacterController.vThirdPersonInput::StrafeInput()
extern void vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD (void);
// 0x00000085 System.Void Invector.vCharacterController.vThirdPersonInput::SprintInput()
extern void vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57 (void);
// 0x00000086 System.Boolean Invector.vCharacterController.vThirdPersonInput::JumpConditions()
extern void vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4 (void);
// 0x00000087 System.Void Invector.vCharacterController.vThirdPersonInput::JumpInput()
extern void vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5 (void);
// 0x00000088 System.Void Invector.vCharacterController.vThirdPersonInput::.ctor()
extern void vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB (void);
// 0x00000089 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isStrafing()
extern void vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791 (void);
// 0x0000008A System.Void Invector.vCharacterController.vThirdPersonMotor::set_isStrafing(System.Boolean)
extern void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE (void);
// 0x0000008B System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isGrounded()
extern void vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB (void);
// 0x0000008C System.Void Invector.vCharacterController.vThirdPersonMotor::set_isGrounded(System.Boolean)
extern void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B (void);
// 0x0000008D System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isSprinting()
extern void vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD (void);
// 0x0000008E System.Void Invector.vCharacterController.vThirdPersonMotor::set_isSprinting(System.Boolean)
extern void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749 (void);
// 0x0000008F System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_stopMove()
extern void vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58 (void);
// 0x00000090 System.Void Invector.vCharacterController.vThirdPersonMotor::set_stopMove(System.Boolean)
extern void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8 (void);
// 0x00000091 System.Void Invector.vCharacterController.vThirdPersonMotor::Init()
extern void vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37 (void);
// 0x00000092 System.Void Invector.vCharacterController.vThirdPersonMotor::UpdateMotor()
extern void vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483 (void);
// 0x00000093 System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4 (void);
// 0x00000094 System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3)
extern void vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55 (void);
// 0x00000095 System.Void Invector.vCharacterController.vThirdPersonMotor::CheckSlopeLimit()
extern void vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6 (void);
// 0x00000096 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToPosition(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631 (void);
// 0x00000097 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82 (void);
// 0x00000098 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3,System.Single)
extern void vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A (void);
// 0x00000099 System.Void Invector.vCharacterController.vThirdPersonMotor::ControlJumpBehaviour()
extern void vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189 (void);
// 0x0000009A System.Void Invector.vCharacterController.vThirdPersonMotor::AirControl()
extern void vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676 (void);
// 0x0000009B System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_jumpFwdCondition()
extern void vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD (void);
// 0x0000009C System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGround()
extern void vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8 (void);
// 0x0000009D System.Void Invector.vCharacterController.vThirdPersonMotor::ControlMaterialPhysics()
extern void vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01 (void);
// 0x0000009E System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGroundDistance()
extern void vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9 (void);
// 0x0000009F System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle()
extern void vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7 (void);
// 0x000000A0 System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngleFromDirection()
extern void vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F (void);
// 0x000000A1 System.Void Invector.vCharacterController.vThirdPersonMotor::.ctor()
extern void vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D (void);
// 0x000000A2 System.Void Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::.ctor()
extern void vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90 (void);
static Il2CppMethodPointer s_methodPointers[162] = 
{
	vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454,
	vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6,
	vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6,
	vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7,
	vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7,
	vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144,
	vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57,
	vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2,
	vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2,
	vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6,
	vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070,
	vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4,
	vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F,
	NM_Wind_Start_m5AD8914FCA79D5988FC56B7BAD1C6E1BABCF4A88,
	NM_Wind_Update_m667D6826096516C86964269423A7A22EEEC8D164,
	NM_Wind_OnValidate_m34684340FF474FE412309030FBA8FAAF70DC65CC,
	NM_Wind_ApplySettings_mA0F98E67AC89B7E1D60348A382244B272C4DBA17,
	NM_Wind_GetDirectionAndSpeed_m03651A4FEA264BAB2EA453BBCA128D0F4AB00C02,
	NM_Wind__ctor_mB36F8772CEBFF30CF9732C1A1B245449F7470264,
	RotateGameObject_Start_m86E4C1345156019C6DD3344CFBC593502CCF7BA1,
	RotateGameObject_FixedUpdate_m74844D804AED248B8F292C3876F0A0ABDA818FF1,
	RotateGameObject__ctor_m1E3B1D5EF4FD3337D946B6AE7CCACC5DC66B2735,
	FallDeath_OnTriggerEnter_m7FB4AF593637C636B6A356DEFDF376E34C253044,
	FallDeath__ctor_m3B9B4602FB44CC1B0B5FD95B9F6DA27DB37373BA,
	Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157,
	Controller_timerStart_m8FC16BAFC6C835CB57CCF74DFD09D3A4E1C11474,
	Controller_won_mFCB56D38EE427E6C8A0B9B0A0B1B44559208D9C9,
	Controller_MainMenu_m8B8DB6DD378C27822D96DED01FAC70530B371BFA,
	Controller_Restart_m8C4712D53B64537760E77F6376504A12D3ACE414,
	Controller_pause_mE0DA7BCC01AC8F771748B14266E573DE187A95E7,
	Controller_resume_m580CC5F549CCCD80C950D5028B70E8E6BAF21D06,
	Controller_CheckpointReset_mC3D11518DB641B97E8084485E5B3A10F3A30E6AC,
	Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0,
	Controller_FormatTime_m0D417D2D91E0A57A7558750CF44E9CC3A2E90776,
	Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7,
	WonScreenTrigger_OnTriggerEnter_mA119AD5F27BF9BBE93F2F40AD0FF0C2969451CA8,
	WonScreenTrigger__ctor_m4559CE0ADA19F2F9FD1A6AD9DD9181A6C858AA84,
	Goal_ActivateGameObject_m0963B47EA91627A061C952581F2D155DD6C77E58,
	Goal_turnOffAgain_m5BB614848C4F83F0208B481072625A8E59853F72,
	Goal_OnTriggerEnter_m1E8C1505F29010BEE13A9710AFDC79D08C9AF671,
	Goal__ctor_m514574E513A17F8F4815CDC89A625EA405039239,
	U3CturnOffAgainU3Ed__2__ctor_m32CB265E1C0A1BBE36E815446BF016B20485AA06,
	U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m62376BA17CD17D61F28ECF78D1C9795943A024CC,
	U3CturnOffAgainU3Ed__2_MoveNext_mD79EF42793AEB1CEB7E6D01793A66D19AEE3816C,
	U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC252585D779D8FA7B7181F8351C1D41AD060A7D9,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_m75CEF9472B580421D306233C4CCFFD4A39EF847C,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_mEC0F6C089E8328E215800F30615664394B8C1674,
	Level2Teleport_SwitchScene_m708C82060005CB82C476EF03535B809E1AEADAAB,
	Level2Teleport_OnTriggerEnter_mB76C5EFA2EDBC88C5B45BACCD6AD1CFF1D578AA6,
	Level2Teleport__ctor_mFB32379590C6C963B4C2757D11E70896A60A2252,
	Lift_OnTriggerEnter_mE790F569FEB64383D3C27CA7CBECBA38DAB39BC6,
	Lift_OnTriggerExit_m57E09B2F00AB87CC74093FF18FC9D7D8E95CFE5E,
	Lift__ctor_m0FBFBCF032FA5111604D4D7193C5E70D07754E01,
	Title_Start_m31B7E4E0F67327EE18202AD00430FB7000A526ED,
	Title_turnOffAgain_mB96F1A75FC6BDEDD17616E23DF8FC8C2D5003833,
	Title__ctor_mF2CBA7A07A8BDFAF5453BC101694B429F9A5F7D1,
	U3CturnOffAgainU3Ed__2__ctor_m374C026397F29CC566F0B8DA9537463FA1892D35,
	U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m69CE49C38783CCBCE7B601C40D57750C06BD700B,
	U3CturnOffAgainU3Ed__2_MoveNext_mBE215163D7C39BD10602A4071FE65411752AC3E4,
	U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE19ECE269C4F22788B995A16F85AB1D49756146,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_m7BE9F4C59A51008962B0EFB0FA7A544159DE3267,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_m5EB8A43D4E5E9C35C448CC6FF516070E0719FD7B,
	CheckpointSave_Start_m4562257ABF964E696BA98A7270EE3DD118450BE8,
	CheckpointSave_saveNew_m8D3F1694990623CCE10E743B206C3C42C142F669,
	CheckpointSave_Reset_mF243B781338FB4314316199762D4B5CB62129D09,
	CheckpointSave__ctor_m2CD777C615911A86F52DD3EEA8A2FEB1D58630B6,
	CheckpointTrigger_OnTriggerEnter_m90A6BA7B9E420368E50AF84B86F71E0AEB24CF76,
	CheckpointTrigger__ctor_mC50DCFF72998CD137E36196B130A6821C1E6ABFF,
	FlyingEnemies_OnTriggerEnter_m4F85ABDD204E193798D2AA75713FA8FD34099440,
	FlyingEnemies__ctor_m1F98CE74FBA4C059E1754066031C6B928E893509,
	Title2_Start_m1BA01B9BF0E3685F6548A8370525F6703CBF0DFA,
	Title2_turnOffAgain_mDD8D5B4DB6E71E2C3101DAAD52F34E4929B0BF8E,
	Title2__ctor_m73D6720BD9BA5CFCF3E723904D0176A024FDFD57,
	U3CturnOffAgainU3Ed__2__ctor_mF695D8FC80AEEADE2207887350ED99BF20E82AFC,
	U3CturnOffAgainU3Ed__2_System_IDisposable_Dispose_m943EC455D02E031857134967F6327F4B78F9A1C9,
	U3CturnOffAgainU3Ed__2_MoveNext_m4FD12C5D49634FD95ACD0A2DB1F57F607CC1C784,
	U3CturnOffAgainU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD56B68D16C56A39059F342879F2E8C197B942D89,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_Reset_mB8D1E8F46164E4FBDC13093A9FE31963C45C71AA,
	U3CturnOffAgainU3Ed__2_System_Collections_IEnumerator_get_Current_mE97EA85905F53D8519416505980BD13CF173CB79,
	MainMenu_SwitchScene_mA606486AA43867C8F874C56251EC9FD109428C1D,
	MainMenu_CloseGame_m595F4E0AC6AF40ABAF8A5466A7931B799C45259F,
	MainMenu_HowToPlay_m0DCA1FF77E60CBEBAB42B1D2F548F9CD908B512E,
	MainMenu_HowToPlayBack_mEC7F54C3A215F61BB40A4B5EFE634170E002F172,
	MainMenu_Settings_m2CA6225F651EF0A8B1FAB410C7158654D548B5DA,
	MainMenu_SettingsBack_mC4A92A9091DB48A3DD5BD7AD9DFEE557347C61B6,
	MainMenu_About_m14A470171CAB4DA6AF1D39168D15635658FA5B0F,
	MainMenu_AboutBack_m88DB6F8B446CDA924501C96FCC0F00236D54E70C,
	MainMenu_Levels_mE65AB9B148AFBE616CFF9F5FC7F1D00A1AB1ED6C,
	MainMenu_LevelsBack_m4FA837F4DCE46BE4A99D52CC704C706CB3E2481F,
	MainMenu_GitHub_m905535F114753813DF6EDD2C9F541195168EA4B8,
	MainMenu_GitLab_mCDFE6F3CEDE5BCFC7D3DE1C2D8CEABE8583C4E92,
	MainMenu_itch_m019DACDB991A35F957F91CC8171F80A78FEB932D,
	MainMenu_RickRoll_mAC7939B6A257D04543BC1AB775CEDA4995DC4157,
	MainMenu_Jebaited_m5FD1F7B71C3F9E9BA67CC9BE68F58020CFB810DF,
	MainMenu_Level1_m98D042A3C61CBD8F9A9370DDEC06CE247D4106F1,
	MainMenu_Level2_m72C6F165FAC5D290F0AE152BA1C2B561C267B959,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	LowPolyWater_Awake_mBF6D32C683E8BD39148DB6E7AB59FA73EAB6B1C9,
	LowPolyWater_Start_m89AFA69FF7508A8D612A6C5E5DACB95F2B94AC5B,
	LowPolyWater_CreateMeshLowPoly_mE6F5928759402E4367E29A84DB1B4C8FFAAE63D0,
	LowPolyWater_Update_m5E2FF7C7587E28FA08EA29B9BDD359F190156CA9,
	LowPolyWater_GenerateWaves_m128D40285442A523C43AAB1BA22EF0E21544CEEE,
	LowPolyWater__ctor_m616A0E96E64BD42A2ECBAB3FE69EE017057502DE,
	vAnimateUV_Update_mFBBF4618D6C5A03EA338C7F81A15BD54F92E5C6B,
	vAnimateUV__ctor_m5DA31CAF76CFC8D3439AA7AA3DCAA989EDE06C3F,
	NULL,
	NULL,
	vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B,
	vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772,
	vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF,
	vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E,
	vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02,
	vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A,
	vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B,
	vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D,
	vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36,
	vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A,
	vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7,
	vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC,
	vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189,
	vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8,
	vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164,
	vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080,
	vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33,
	vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633,
	vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117,
	vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1,
	vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8,
	vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875,
	vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830,
	vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576,
	vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD,
	vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57,
	vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4,
	vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5,
	vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB,
	vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791,
	vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE,
	vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB,
	vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B,
	vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD,
	vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749,
	vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58,
	vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8,
	vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37,
	vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483,
	vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4,
	vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55,
	vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6,
	vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631,
	vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82,
	vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A,
	vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189,
	vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676,
	vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD,
	vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8,
	vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01,
	vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9,
	vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7,
	vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F,
	vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D,
	vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90,
};
static const int32_t s_InvokerIndices[162] = 
{
	1365,
	1365,
	1365,
	1129,
	1129,
	880,
	698,
	1365,
	77,
	1365,
	1365,
	1129,
	1365,
	1365,
	1365,
	1365,
	1365,
	1362,
	1365,
	1365,
	1365,
	1365,
	1129,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	870,
	1365,
	1129,
	1365,
	1365,
	1321,
	1129,
	1365,
	1120,
	1365,
	1343,
	1321,
	1365,
	1321,
	1365,
	1129,
	1365,
	1129,
	1129,
	1365,
	1365,
	1321,
	1365,
	1120,
	1365,
	1343,
	1321,
	1365,
	1321,
	1365,
	1165,
	1365,
	1365,
	1129,
	1365,
	1129,
	1365,
	1365,
	1321,
	1365,
	1120,
	1365,
	1343,
	1321,
	1365,
	1321,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	868,
	1365,
	1365,
	1365,
	1365,
	1365,
	-1,
	-1,
	1819,
	1713,
	1365,
	1365,
	1129,
	1365,
	2264,
	1365,
	1365,
	1365,
	1129,
	1147,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1365,
	1343,
	1365,
	1365,
	1343,
	1147,
	1343,
	1147,
	1343,
	1147,
	1343,
	1147,
	1365,
	1365,
	1129,
	1165,
	1365,
	1165,
	1165,
	704,
	1365,
	1365,
	1343,
	1365,
	1365,
	1365,
	1346,
	1346,
	1365,
	1365,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0600006A, { 0, 1 } },
	{ 0x0600006B, { 1, 3 } },
};
extern const uint32_t g_rgctx_TU5BU5D_t707AF8CE8084AD409837026C4FF156FA9D81C2B7;
extern const uint32_t g_rgctx_List_1_get_Count_mFF417D66F4D963BEFC5C340EA1F9AD3E018F7531;
extern const uint32_t g_rgctx_TU5BU5D_t57ACF23387F5A87058D4FBB5157489E72E540C19;
extern const uint32_t g_rgctx_List_1_get_Item_m64D10518960225DC0FDA45ED8629C162DB9D1A98;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t707AF8CE8084AD409837026C4FF156FA9D81C2B7 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_mFF417D66F4D963BEFC5C340EA1F9AD3E018F7531 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_TU5BU5D_t57ACF23387F5A87058D4FBB5157489E72E540C19 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_m64D10518960225DC0FDA45ED8629C162DB9D1A98 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	162,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
