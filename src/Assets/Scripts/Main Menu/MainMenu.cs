using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //Main Menu
    public void SwitchScene()
    {
        SceneManager.LoadScene(1);
        Cursor.visible = false;
    }
    
    public void CloseGame()
    {
        Application.Quit();
    }

    //Transitions menu screens
    public Animator animator;
    public void HowToPlay()
    {
        animator.SetBool("HowToPlay",true);
    }

    public void HowToPlayBack()
    {
        animator.SetBool("HowToPlay",false);
    }

    public void Settings()
    {
        animator.SetBool("Settings",true);
    }

    public void SettingsBack()
    {
        animator.SetBool("Settings",false);
    }

    public void About()
    {
        animator.SetBool("About",true);
    }

    public void AboutBack()
    {
        animator.SetBool("About",false);
    }

    public void Levels()
    {
        animator.SetBool("Levels",true);
    }

    public void LevelsBack()
    {
        animator.SetBool("Levels",false);
    }

    //About
    public void GitHub()
    {
        Application.OpenURL ("https://github.com/MagicLike");
    }
    public void GitLab()
    {
        Application.OpenURL ("https://gitlab.com/MagicLike");
    }
    public void itch()
    {
        Application.OpenURL ("https://magiclike.itch.io/");
    }

    //Settings
    public void RickRoll()
    {
        Application.OpenURL ("https://youtu.be/dQw4w9WgXcQ");
    }

    public void Jebaited()
    {
        Application.OpenURL ("https://youtu.be/d1YBv2mWll0");
    }

    //Levels
    public void Level1()
    {
        GameObject.Find("Controller").GetComponent<Controller>().level = 1;
        SceneManager.LoadScene(GameObject.Find("Controller").GetComponent<Controller>().level);
        Cursor.visible = false;
    }

    public void Level2()
    {
        GameObject.Find("Controller").GetComponent<Controller>().level = 2;
        SceneManager.LoadScene(GameObject.Find("Controller").GetComponent<Controller>().level);
        Cursor.visible = false;
    }
}