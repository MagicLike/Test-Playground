using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallDeath : MonoBehaviour
{
    //public void SwitchScene()
    //{
    //    SceneManager.LoadScene(GameObject.Find("Controller").GetComponent<Controller>().level);
    //}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        other.GetComponent<CheckpointSave>().Reset();
    }
}
