using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Controller : MonoBehaviour
{
    private static Controller controllerInstance;
    void Start()
    {
        DontDestroyOnLoad(this);
        if(controllerInstance != null)
        {
            Destroy(controllerInstance.gameObject);
        }
        controllerInstance = this;
    }

    public bool started = false;
    public void timerStart()
    {
        started = true;
    }

    public string timerText;
    public float time;
    public Text textObject;

    public GameObject WonPanel;
    public Text WonTime;

    public void won()
    {
        started = false;
        Cursor.visible = true;
        WonPanel.SetActive(true);
        WonTime.text = FormatTime(time);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
        Cursor.visible = true;
    }

    public int level = 1;

    public void Restart()
    {
       pauseMenu.SetActive(false);
       WonPanel.SetActive(false);
       Cursor.visible = false;
       SceneManager.LoadScene(level);
       time = 0f;
       started = true; 
    }


    public GameObject pauseMenu;  

    public void pause()
    {
        started = false;
        pauseMenu.SetActive(true);
        Cursor.visible = true;
    }

    public void resume()
    {
        pauseMenu.SetActive(false);
        started = true;
        Cursor.visible = false;
    }

    public void CheckpointReset()
    {
        GameObject.FindWithTag("Player").GetComponent<CheckpointSave>().Reset();
        pauseMenu.SetActive(false);
        started = true;
        Cursor.visible = false;
    }

    void Update()
    {
        if (started == true && Input.GetKeyDown(KeyCode.Escape))
        {
           pause();
        } else 

        if (started == false && Input.GetKeyDown(KeyCode.Escape))
        {
            resume();
        }

        if(!started) return;

        time += Time.deltaTime;
        if(time < 0)
        {
            time = 0f;
            SceneManager.LoadScene(0);
            started = false;
        }

        textObject.text = FormatTime(time);
    }

    string FormatTime(float time)
    {
        int intTime = (int)time;
        int minutes = intTime / 60;
        int seconds = intTime % 60;
        float fraction = time * 1000;
        fraction = (fraction % 1000);
        string timeText = String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, fraction);
        return timeText;
    }
}
