using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WonScreenTrigger : MonoBehaviour
{
  private void OnTriggerEnter(Collider other)
    {
 
        if (other.gameObject.tag == "Player")
            GameObject.Find("Controller").
                GetComponent<Controller>().won();
 
    }
}