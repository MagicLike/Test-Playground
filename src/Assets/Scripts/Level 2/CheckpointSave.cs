using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CheckpointSave : MonoBehaviour
{
    public Vector3 saveSpot;
    void Start()
    {
        saveSpot = transform.position;
    }

    public void saveNew(Vector3 newsavepos)
    {
        saveSpot = newsavepos;
    }

    public void Reset()
    {
        transform.position = saveSpot;

        GetComponent<vThirdPersonController>().Init();
    }
}
