using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title2 : MonoBehaviour
{
    public Text myTextField;
    void Start()
    {
        myTextField.text = "Level 2";
        StartCoroutine(turnOffAgain());
    }
    IEnumerator turnOffAgain()
    {
        yield return new WaitForSeconds(3f);
        myTextField.text = "";
    }
}
