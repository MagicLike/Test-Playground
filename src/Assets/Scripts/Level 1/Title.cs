using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour
{
    public Text myTextField;
    void Start()
    {
        myTextField.text = "Level 1";
        StartCoroutine(turnOffAgain());
    }
    IEnumerator turnOffAgain()
    {
        yield return new WaitForSeconds(3f);
        myTextField.text = "";
    }
}
