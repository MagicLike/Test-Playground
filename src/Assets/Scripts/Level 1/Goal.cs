using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public GameObject go;
    public void ActivateGameObject()
    {
        go.SetActive(true);
        StartCoroutine(turnOffAgain());
    }

    IEnumerator turnOffAgain()
    {
        yield return new WaitForSeconds(2f);
        go.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        ActivateGameObject();
        
    }
}
