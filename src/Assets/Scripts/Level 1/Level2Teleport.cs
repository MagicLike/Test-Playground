using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2Teleport : MonoBehaviour
{
   public void SwitchScene()
    {
        GameObject.Find("Controller").GetComponent<Controller>().level += 1;
        SceneManager.LoadScene(GameObject.Find("Controller").GetComponent<Controller>().level);
    } 
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        SwitchScene();
    }
}
