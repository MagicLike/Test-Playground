# Test Playground

- [About](#about)
- [**Download**](#download)
- [Screenshots](#screenshots)
- [Donate](#donate)


<br>
<div id="about">

This is the source code for my first own game I created, called "Test Playground".

I decided to publish the source code, to get more people into coding or more paticular game design, even if the game design of my own is just trash. (OK, it was also made within 5 days)

The current Unity version is 2021.1.22f1.

<br>

## So what the heck is this game?

Something strange, with no clear object.

It's based on the idea to make a jump 'n' run game, but I just put in everything I liked in between... xD

I also learned game design (at least the basics) with this game, so just let it exist, it's not perfect! (very buggy)

</div>
<br>

## Download

[<img src="./assets/available-on-itch.io.svg" alt="Available on itch.io" width="150px">](https://magiclike.itch.io/test-playground)

<br>

# Screenshots
<img  src="./assets/MainMenu.png"  alt="Main menu"  width="300">
<img  src="./assets/HowToPlay.png"  alt="How to play "  width="300">
<img  src="./assets/Level1Start.png"  alt="Start of Level 1"  width="300"> 
<img  src="./assets/Level1.png"  alt="Level 1"  width="300">
<img  src="./assets/Level2Start.png"  alt="Start of Level 2"  width="300">
<img  src="./assets/Level2.png"  alt="Level 2"  width="300">

<br>

# Donate

If you think this code was useful to you or you just want to appreciate my 5-day crashcourse in Game developement, you can donate to me [here](https://magiclike.codeberg.page/#donate).